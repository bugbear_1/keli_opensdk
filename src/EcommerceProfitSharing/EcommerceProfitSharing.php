<?php
namespace Keli\OpenSDK\EcommerceProfitSharing;


use Keli\OpenSDK\Core\Api;

class EcommerceProfitSharing extends Api
{
    /**
     * 添加分账接收方
     * @param array $appid
     * @param String $type
     * @param String $account
     * @param String $name
     * @param String|null $encrypted_name
     * @param String|null $relation_type
     * @return \Psr\Http\Message\ResponseInterface|string
     */
    public function addReceiver(String $appid, String $type, String $account, String $name, String $sub_mchid, String $encrypted_name = null, String $relation_type = null){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/addReceiver',[
            'appid' => $appid,
            'sub_mchid' => $sub_mchid,
            'type' => $type,
            'account' => $account,
            'name' => $name,
            'encrypted_name' => $encrypted_name,
            'relation_type' => $relation_type
        ]);
    }


    /**
     * 请求分账
     * @param String $appid
     * @param String $sub_mchid
     * @param array $transaction_id
     * @param String $out_order_no
     * @param array $receivers
     * @param String $finish
     * @return array|\Psr\Http\Message\ResponseInterface|string
     */
    public function doProfitsharing(String $appid, String $sub_mchid, String $transaction_id, String $out_order_no, Array $receivers, String $finish){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/doProfitsharing',[
            'appid' => $appid,
            'sub_mchid' => $sub_mchid,
            'transaction_id' => $transaction_id,
            'out_order_no' => $out_order_no,
            'receivers' => $receivers,
            'finish' => $finish
        ]);
    }


    /**
     * 查询订单剩余待分金额
     * @param string $transaction_id
     * @return array|\Psr\Http\Message\ResponseInterface|string
     */
    public function getRemainingAmount(string $transaction_id) {
        return $this->request('pay/wechatPay/ecommerceProfitSharing/getRemainingAmount',[
            'transaction_id' => $transaction_id,
        ]);
    }

    /**
     * 查询分账结果
     */
    public function getReceiver(String $sub_mchid, String $transaction_id, String $out_order_no = null){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/getReceiver',[
            'sub_mchid' => $sub_mchid,
            'transaction_id' => $transaction_id,
            'out_order_no' => $out_order_no
        ]);
    }
    /**
     * 查询二级商户账户实时余额
     */
    public function doFundBalance(String $sub_mchid){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/doFundBalance',[
            'sub_mchid' => $sub_mchid,
        ]);
    }

    /**
     * 分账退回
     */
    public function doReturnOrders(String $sub_mchid, String $out_return_no, String $return_mchid, String $amount, String $description, String $order_id = null, String $out_order_no = null){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/doReturnOrders',[
            'sub_mchid' => $sub_mchid,
            'out_return_no' => $out_return_no,
            'return_mchid' => $return_mchid,
            'amount' => $amount,
            'description' => $description,
            'order_id' => $order_id,
            'out_order_no' => $out_order_no
        ]);
    }

    /**
     * 请求退款
     */
    public function doRefundsApply(String $sub_mchid, String $out_trade_no, String $out_refund_no, String $amount, String $reason, String $sp_appid = null){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/doRefundsApply',[
            'sub_mchid' => $sub_mchid,
            'sp_appid' => $sp_appid,
            'out_trade_no' => $out_trade_no,
            'out_refund_no' => $out_refund_no,
            'amount' => $amount,
            'reason' => $reason
        ]);
    }

    /**
     * 请求补差
     */
    public function doSubsidiesApply(String $sub_mchid, String $transaction_id, String $out_subsidy_no, String $amount, String $description, String $refund_id = null){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/doSubsidiesApply',[
            'sub_mchid' => $sub_mchid,
            'transaction_id' => $transaction_id,
            'out_subsidy_no' => $out_subsidy_no,
            'amount' => $amount,
            'description' => $description,
            'refund_id' => $refund_id
        ]);
    }

    /**
     * 补差退回
     */
    public function doSubsidiesReturn(String $sub_mchid, String $out_order_no, String $transaction_id, String $amount, String $description, String $refund_id = null){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/doSubsidiesReturn',[
            'sub_mchid' => $sub_mchid,
            'out_order_no' => $out_order_no,
            'transaction_id' => $transaction_id,
            'refund_id' => $refund_id,
            'amount' => $amount,
            'description' => $description
        ]);
    }

    /**
     * 二级商户余额提现
     */
    public function doFundWithdraw(String $sub_mchid, String $out_request_no, String $amount, String $remark = null, String $bank_memo = null, String $account_type = null){
        return $this->request('pay/wechatPay/ecommerceProfitSharing/doFundWithdraw',[
            'sub_mchid' => $sub_mchid,
            'out_request_no' => $out_request_no,
            'amount' => $amount,
            'remark' => $remark,
            'bank_memo' => $bank_memo,
            'account_type' => $account_type
        ]);
    }



    /**
     * Alipay添加分账接收方
     */
    public function alipayRelationBind(Array $receiver_list, String $out_request_no, String $app_auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationBind',[
            'receiver_list' => $receiver_list,
            'out_request_no' => $out_request_no,
            'app_auth_token' => $app_auth_token
        ]);
    }

    /**
     * Alipay解绑分账接收方
     */
    public function alipayRelationUnbind(String $receiver_list, String $out_request_no, String $app_auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationUnbind',[
            'receiver_list' => $receiver_list,
            'out_request_no' => $out_request_no,
            'app_auth_token' => $app_auth_token
        ]);
    }

    /**
     * Alipay分账关系查询
     */
    public function alipayRelationBatchQuery(String $out_request_no, String $app_auth_token = null, String $page_num = null, String $page_size = null){
        return $this->request('pay/aliPay/profitSharing/relationBatchQuery',[
            'out_request_no' => $out_request_no,
            'app_auth_token' => $app_auth_token,
            'page_num' => $page_num,
            'page_size' => $page_size
        ]);
    }

    /**
     * Alipay统一收单交易结算接口
     */
    public function alipayRelationSettle(String $trade_no, String $out_request_no, String $royalty_parameters, String $operator_id = null, String $app_auth_token = null, String $extend_params = null){
        return $this->request('pay/aliPay/profitSharing/relationSettle',[
            'app_auth_token' => $app_auth_token,
            'trade_no' => $trade_no,
            'out_request_no' => $out_request_no,
            'royalty_parameters' => $royalty_parameters,
            'operator_id' => $operator_id,
            'extend_params' => $extend_params
        ]);
    }

    /**
     * Alipay统一收单线下交易查询
     */
    public function alipayRelationTradeQuery(String $trade_no, String $out_request_no, String $app_auth_token = null, String $org_pid = null, String $query_options = null){
        return $this->request('pay/aliPay/profitSharing/relationTradeQuery',[
            'app_auth_token' => $app_auth_token,
            'trade_no' => $trade_no,
            'out_request_no' => $out_request_no,
            'org_pid' => $org_pid,
            'query_options' => $query_options
        ]);
    }

    /**
     * Alipay分账退回
     */
    public function alipayRelationTradeRefund(String $trade_no, String $refund_amount, String $app_auth_token = null, String $out_trade_no = null, String $refund_reason = null, String $out_request_no = null, String $refund_royalty_parameters = null, String $query_options = null){
        return $this->request('pay/aliPay/profitSharing/relationTradeRefund',[
            'app_auth_token' => $app_auth_token,
            'trade_no' => $trade_no,
            'refund_amount' => $refund_amount,
            'out_trade_no' => $out_trade_no,
            'refund_reason' => $refund_reason,
            'out_request_no' => $out_request_no,
            'refund_royalty_parameters' => $refund_royalty_parameters,
            'query_options' => $query_options
        ]);
    }


    /**
     * Alipay直付通资金结算
     */
    public function alipayZftTradeSettleConfirm(String $zft_config,String $out_request_no, String $trade_no , String $settle_info = null, String $extend_params = null, String $app_auth_token = null){
        return $this->request('pay/aliPay/zftProfitSharing/tradeSettleConfirm',[
            'zft_config' => $zft_config,
            'app_auth_token' => $app_auth_token,
            'out_request_no' => $out_request_no,
            'trade_no' => $trade_no,
            'settle_info' => $settle_info,
            'extend_params' => $extend_params
        ]);
    }
    /**
     * Alipay直付通分账、补差
     */
    public function alipayZftRelationSettle(String $zft_config,String $out_request_no, String $trade_no , String $royalty_parameters = null, String $operator_id = null, String $extend_params = null, String $app_auth_token = null){
        return $this->request('pay/aliPay/zftProfitSharing/relationSettle',[
            'zft_config' => $zft_config,
            'app_auth_token' => $app_auth_token,
            'out_request_no' => $out_request_no,
            'trade_no' => $trade_no,
            'royalty_parameters' => $royalty_parameters,
            'operator_id' => $operator_id,
            'extend_params' => $extend_params,
        ]);
    }
    /**
     * Alipay直付通分账、补差状态查询
     */
    public function alipayZftTradeQuery(String $zft_config,String $out_request_no, String $trade_no , String $org_pid = null, String $query_options = null, String $app_auth_token = null){
        return $this->request('pay/aliPay/zftProfitSharing/tradeQuery',[
            'zft_config' => $zft_config,
            'app_auth_token' => $app_auth_token,
            'out_request_no' => $out_request_no,
            'trade_no' => $trade_no,
            'org_pid' => $org_pid,
            'query_options' => $query_options
        ]);
    }

}