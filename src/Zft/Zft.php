<?php
namespace Keli\OpenSDK\Zft;


use Keli\OpenSDK\Core\Api;

class Zft extends Api
{
    /**
     * Alipay直付通资金结算
     */
    public function alipayZftTradeSettleConfirm(String $out_request_no, String $trade_no , String $settle_info = null, String $extend_params = null, String $app_auth_token = null){
        return $this->request('pay/aliPay/zftProfitSharing/tradeSettleConfirm',[
            'app_auth_token' => $app_auth_token,
            'out_request_no' => $out_request_no,
            'trade_no' => $trade_no,
            'settle_info' => $settle_info,
            'extend_params' => $extend_params
        ]);
    }
    /**
     * Alipay直付通分账、补差
     */
    public function alipayZftRelationSettle(String $out_request_no, String $trade_no , String $royalty_parameters = null, String $operator_id = null, String $extend_params = null, String $app_auth_token = null){
        return $this->request('pay/aliPay/zftProfitSharing/relationSettle',[
            'app_auth_token' => $app_auth_token,
            'out_request_no' => $out_request_no,
            'trade_no' => $trade_no,
            'royalty_parameters' => $royalty_parameters,
            'operator_id' => $operator_id,
            'extend_params' => $extend_params,
        ]);
    }
    /**
     * Alipay直付通分账、补差状态查询
     */
    public function alipayZftTradeQuery(String $out_request_no, String $trade_no , String $org_pid = null, String $query_options = null, String $app_auth_token = null){
        return $this->request('pay/aliPay/zftProfitSharing/tradeQuery',[
            'app_auth_token' => $app_auth_token,
            'out_request_no' => $out_request_no,
            'trade_no' => $trade_no,
            'org_pid' => $org_pid,
            'query_options' => $query_options
        ]);
    }

}