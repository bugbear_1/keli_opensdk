<?php
namespace Keli\OpenSDK\ProfitSharing;


use Keli\OpenSDK\Core\Api;

class ProfitSharing extends Api
{
    /**
     * 添加分账接收方
     * @param array $receiver_list
     * @param String $out_request_no
     * @param String|null $auth_token
     * @return \Psr\Http\Message\ResponseInterface|string
     * @throws \Exception
     */
    public function relationBind(Array $receiver_list, String $out_request_no, String $app_auth_token = null, String $auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationBind',[
            'app_auth_token' => $app_auth_token,
            'auth_token' => $auth_token,
            'receiver_list' => $receiver_list,
            'out_request_no' => $out_request_no
        ]);
    }

    /**
     * 解绑分账接收方
     * @param array $receiver_list
     * @param String $out_request_no
     * @param String|null $auth_token
     * @return \Psr\Http\Message\ResponseInterface|string
     * @throws \Exception
     */
    public function relationUnbind(Array $receiver_list, String $out_request_no,String $auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationUnbind',[
            'app_auth_token' => $app_auth_token,
            'auth_token' => $auth_token,
            'receiver_list' => $receiver_list,
            'out_request_no' => $out_request_no
        ]);
    }

    /**
     * 分账关系查询
     * @param String $out_request_no
     * @param String|null $auth_token
     * @param Int|null $page_num
     * @param Int|null $page_size
     * @return \Psr\Http\Message\ResponseInterface|string
     * @throws \Exception
     */
    public function relationBatchQuery( String $out_request_no, Int $page_num = null, Int $page_size  = null, String $app_auth_token = null, String $auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationUnbind',[
            'app_auth_token' => $app_auth_token,
            'auth_token' => $auth_token,
            'page_num' => $page_num,
            'page_size' => $page_size,
            'out_request_no' => $out_request_no
        ]);
    }


    /**
     * 统一收单交易结算接口
     * @param String $out_request_no
     * @param String $trade_no
     * @param array $royalty_parameters
     * @param String|null $operator_id
     * @param array $extend_params
     * @param String|null $auth_token
     * @return \Psr\Http\Message\ResponseInterface|string
     * @throws \Exception
     */
    public function relationSettle(String $out_request_no, String $trade_no, Array $royalty_parameters, String $operator_id = null, Array $extend_params = [], String $app_auth_token = null, String $auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationSettle',[
            'app_auth_token' => $app_auth_token,
            'auth_token' => $auth_token,
            'out_request_no' => $out_request_no,
            'trade_no' => $trade_no,
            'royalty_parameters' => $royalty_parameters,
            'operator_id' => $operator_id,
            'extend_params' => $extend_params
        ]);
    }



    /**
     * 统一收单线下交易查询
     * @param String $out_trade_no
     * @param String $trade_no
     * @param String|null $org_pid
     * @param array $query_options
     * @param String|null $auth_token
     * @return \Psr\Http\Message\ResponseInterface|string
     * @throws \Exception
     */
    public function relationTradeQuery(String $out_request_no = null, String $out_trade_no, String $trade_no, String $org_pid = null, Array $query_options = [], String $app_auth_token = null, String $auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationTradeQuery',[
            'app_auth_token' => $app_auth_token,
            'out_request_no' => $out_request_no,
            'auth_token' => $auth_token,
            'out_trade_no' => $out_trade_no,
            'trade_no' => $trade_no,
            'org_pid' => $org_pid,
            'query_options' => $query_options
        ]);
    }

    public function relationTradeOrderSettleQueryRequest(String $settle_no = null, String $app_auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationTradeOrderSettleQueryRequest',[
            'app_auth_token' => $app_auth_token,
            'settle_no' => $settle_no
        ]);
    }

    /**
     * 统一收单交易退款接口
     */
    public function relationTradeRefund(String $out_trade_no, String $trade_no, $refund_amount,String $refund_reason = null, String $out_request_no = null, Array $refund_royalty_parameters = [], Array $query_options = [], String $app_auth_token = null, String $auth_token = null){
        return $this->request('pay/aliPay/profitSharing/relationTradeRefund',[
            'app_auth_token' => $app_auth_token,
            'auth_token' => $auth_token,
            'out_trade_no' => $out_trade_no,
            'trade_no' => $trade_no,
            'refund_amount' => $refund_amount,
            'refund_reason' => $refund_reason,
            'out_request_no' => $out_request_no,
            'refund_royalty_parameters' => $refund_royalty_parameters,
            'query_options' => $query_options
        ]);
    }

}