
### 安装
> composer require keli/opensdk:dev-master

> 目前没有稳定版本,开发测试均使用 dev-master

## 系统目录结构
~~~
├─src                   根目录
│  ├─Card               卡券模块
│  ├─Core               核心类
│  ├─Member             会员模块
│  ├─Pay                支付模块
│  ├─Token              公共Token模块
│  ├─Vehicle            车主服务
│  ├─Wechat             微信模块
│  └─Dispatch.php       入口路由
├─vendor                第三方类库目录（Composer依赖库）
├─LICENSE               开源协议
├─README.md             README 文件
├─composer.json         composer 定义文件
└─composer.lock         composer 定义文件
~~~

### 外网调用配置
| 参数    | 必填 | 示例 |
| --------- | ---- | ---- |
| appId     | 是  | / |
| appSecret | 是  | / |
| debug     | 是  | true |

### 内网调用配置
| 参数    | 必填 | 示例 |
| --------- | ---- | ---- |
| mch_id    | 是  | 1 |
| debug     | 是  | true |


### 调用示例
```php
$configs = [
    'mch_id' => 31,
    'debug'  => true
];

//获取sdk实例
$dispatch = new \Keli\OpenSDK\Dispatch($configs);
//内网调用需指定inner模式
$dispatch->inner = true;

/**
 * 会员模块，查找用户信息示例
 */
 
$userData = $dispatch->member->getMember('18591751341');
if($userData['code'] == 200){
    echo '找到会员';
}

/**
 * 更多模块调用方式
 */
 
$dispatch->card; //卡券服务
$dispatch->pay; //支付服务
$dispatch->member; //会员服务
$dispatch->wechat; //微信服务
$dispatch->vehicle; //车主服务
```

### 版本更新记录

#### 2021-12-28
修正debug模式抛出异常

新增分账补差相关接口

新增若干其他接口

EcommerceProfitSharing下新增三个直付通接口

config支持直接配置inner模式

#### 2020-07-20
增加支付宝分账

#### 2020-06-29
微信模块增加微信分账

#### 2020-06-25
增加微信车主服务模块

#### 2020-06-15
引入GuzzleHttp替换Curl请求方式

优化非Debug模式下的异常抛出